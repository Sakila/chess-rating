#generate_data
import pandas as pd 
import csv

def read_data(file_name):
	data = pd.read_csv(file_name)

	return data


month = ['jan','feb','mar','apr','may','jun','jul','aug','sep','oct','nov','dec']

no_of_files = 59
dictn = {}
year = 2013

for i in range(1,no_of_files+1):
	file_name = 'data/%d/%d%s.csv'%(year,year,month[i%12-1])
	print(file_name)
	if (i%12 ==0):
		year += 1 
	data = read_data(file_name)
	

	id_no = data.ix[:,0]


	for j in range(len(id_no)):
		Name = data['Name'][j]
		Fed = data['Fed'][j]
		Sex = data['Sex'][j]
		B_day = data['B-day'][j]
		if B_day==0:
			continue
		Gms = data['Gms'][j]
		rating = (data.ix[j,6])
		if type(rating) == str:
			print (rating)
			rating = int(rating)
		# rating = int(rating)
 
		Age = (year - B_day) * 12 + i%12
		if id_no[j] in dictn:
			dictn[id_no[j]][4].append(Age)
			dictn[id_no[j]][5].append(rating)
			dictn[id_no[j]][6].append(Gms)

		else:
			dictn[id_no[j]] = [Name,Fed,Sex,B_day,[Age],[rating],[Gms]]




print dictn
import csv

csv = open('data_new.csv', "w") 
#"w" indicates that you're writing strings to the file

columnTitleRow = "id, name, fed, gender, Birth Date, Age, rating, Gms\n"
csv.write(columnTitleRow)
idn = dictn.keys()

for i in range(len(idn)):
	# if dictn[idn[i]][0] == nan
	nameInQuotes = "\"" + str(dictn[idn[i]][0]) + "\""
	AgeIn = "\"" + str(dictn[idn[i]][4]) + "\""
	ratingIn = "\"" + str(dictn[idn[i]][5]) + "\""
	GmsIn = "\"" + str(sum(dictn[idn[i]][6])) + "\""

	row = str(idn[i]) + "," + nameInQuotes + ',' + dictn[idn[i]][1] +',' + dictn[idn[i]][2] +',' + str(dictn[idn[i]][3]) + "," + AgeIn+ "," + ratingIn+ ',' + GmsIn + "\n"
	csv.write(row)
