
import math
import csv
# idn,B_date,rating = [1,2,3,4,5,6,7,8,9,10,11,12,13,14],[1,1,1,1,1,1,1,1,1,1,1,1,2,2],[1302,1220,1172,1157,1140,1140,1138,1082,1074,1073,1027,1015,300,600]


# def get_data(file_name):

def get_data():
	with open('2017nov.csv', 'r') as f:
		idn = []
		B_date =[]
		rating=[]
		name = []
		fed = []
		gender = []
		firstline=True
		for row in csv.reader(f.read().splitlines()):
			# print(row)
			if firstline:
				firstline = False
			else:
				if row[3] == 'F':
					idn.append(row[0])
					name.append(row[1])
					fed.append(row[2])
					gender.append(row[3])
					B_date.append((row[9]))
					rating.append(int(row[6]))
	return idn,B_date,rating,name,fed,gender


idn,B_date,rating,name,fed,gender =get_data()
print rating

temp = {}
for i in range(len(idn)):

	if B_date[i] in temp.keys():
		temp[B_date[i]][0] += 1.0
		n = temp[B_date[i]][0]
		temp[B_date[i]][1] = (temp[B_date[i]][1]*(n-1)+rating[i])/n
		temp[B_date[i]][2] += (rating[i]**2.0)
		temp[B_date[i]][3] = ((temp[B_date[i]][2]/n)-(temp[B_date[i]][1])**2.0)**0.5
		# x += rating[i]
		# x_squre += math.squre(rating[i])

	else:
		temp[B_date[i]] = [1.0]
		temp[B_date[i]].append(rating[i])
		temp[B_date[i]].append(rating[i]**2.0)
		temp[B_date[i]].append(1)
		

print temp


csv = open('test.csv', "w") 
#"w" indicates that you're writing strings to the file

columnTitleRow = "id, name, fed, gender, rating, Birth Date, No of Players, Mean, Std, z_core\n"
csv.write(columnTitleRow)


for i in range(len(idn)):
	count = temp[B_date[i]][0]
	mean = temp[B_date[i]][1]
	std = temp[B_date[i]][3]
	z_core = (rating[i]-mean)/std
	z_core = str(z_core)

	
	nameInQuotes = "\"" + name[i] + "\""

	row = idn[i] + "," + nameInQuotes + ',' + fed[i] +',' + gender[i] +',' + str(rating[i]) + "," + str(B_date[i]) + "," + str(count) +"," + str(mean) +"," + str(std) + ","+ z_core + "\n"
	csv.write(row)

	# print(z_core)
	
	
	